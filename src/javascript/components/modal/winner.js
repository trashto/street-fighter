import {showModal} from './modal'

export function showWinnerModal(fighter) {
  const img = document.createElement('img')
  img.src = fighter.source;
  showModal({title:'You are a monster '+ fighter.name, bodyElement: img})
}
