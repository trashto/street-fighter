import { controls } from "../../constants/controls";

export async function fight(firstFighter, secondFighter) {
  firstFighter.healthIndicator = document.getElementById(
    "left-fighter-indicator"
  );
  secondFighter.healthIndicator = document.getElementById(
    "right-fighter-indicator"
  );

  firstFighter.maxHealth = firstFighter.health;
  secondFighter.maxHealth = secondFighter.health;

  return new Promise((resolve) => {
    // resolve the promise with the winner when fight is over
    const keystrokes = {};

    const doFight = (key) => {
      if (key.repeat) {
        return;
      }

      keystrokes[key.code] = key.type === "keydown";

      handleKeystrokes(keystrokes, firstFighter, secondFighter);
      updateHealth(firstFighter);
      updateHealth(secondFighter);

      if (firstFighter.health <= 0) {
        resolve(secondFighter);
      } else if (secondFighter.health <= 0) {
        resolve(firstFighter);
      }
    };

    document.addEventListener('keydown', doFight);
    document.addEventListener('keyup', doFight);
  });
}

function handleKeystrokes(keystrokes, firstFighter, secondFighter) {

  firstFighter.block = keystrokes[controls.PlayerOneBlock];
  secondFighter.block = keystrokes[controls.PlayerTwoBlock];

  if (keystrokes[controls.PlayerOneAttack]) {
    handleAttack(firstFighter, secondFighter);
    keystrokes[controls.PlayerOneAttack] = false;
  }

  if (keystrokes[controls.PlayerTwoAttack]) {
    handleAttack(secondFighter, firstFighter);
    keystrokes[controls.PlayerTwoAttack] = false;
  }

  if (controls.PlayerOneCriticalHitCombination.every((key) => keystrokes[key])) {
    handleCriticalAttack(firstFighter, secondFighter);
    controls.PlayerOneCriticalHitCombination.forEach((key) => keystrokes[key] = false)
  }

  if (controls.PlayerTwoCriticalHitCombination.every((key) => keystrokes[key])) {
    handleCriticalAttack(secondFighter, firstFighter);
    controls.PlayerTwoCriticalHitCombination.forEach((key) => keystrokes[key] = false)
  }
}

function handleAttack(attacker, defender) {
  if (attacker.block || defender.block) {
    return;
  }
  let attackDamage = getDamage(attacker, defender);
  defender.health -= attackDamage;
}

function handleCriticalAttack(attacker, defender) {
  if (attacker.criticalAttackOnCooldown) {
    return;
  }

  attacker.criticalAttackOnCooldown = true;

  let damage = 2 * attacker.attack;
  defender.health -= damage;

  setTimeout(() => {
    attacker.criticalAttackOnCooldown = false;
  }, 10_000);
}

function updateHealth(player) {
  let width = player.health < 0 ? 0 : (player.health / player.maxHealth) * 100;
  player.healthIndicator.style.width = width + "%";
}

export function getDamage(attacker, defender) {
  // return damage
  const damage = getHitPower(attacker) - getBlockPower(defender);

  if (damage <= 0) {
    return 0;
  }

  return damage;
}

export function getHitPower(fighter) {
  // return hit power
  const criticalHitChance = getRandom(1, 2);
  return fighter.attack * criticalHitChance;
}

export function getBlockPower(fighter) {
  // return block power
  const dodgeChance = getRandom(1, 2);
  return fighter.defense * dodgeChance;
}

function getRandom(min, max) {
  return Math.random() * (max - min) + min;
}
