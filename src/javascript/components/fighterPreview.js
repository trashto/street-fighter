import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });

  // todo: show fighter info (image, name, health, etc.)
  const fighterName = createElement({
    tagName: 'p',
    className: 'fighter-preview___name',
  });
  const fighterDescription = createElement({
    tagName: 'p',
    className: 'fighter-preview___description',
  });
  if (fighter) {
    const fighterImg = createFighterImage(fighter);
    fighterName.innerText = fighter.name;
    fighterName.style.color = 'white';
    fighterName.style.fontSize = '24px';
    fighterName.style.fontFamily = 'Arial';
    fighterName.style.position = 'absolute';
    fighterName.style.margin = '0';
    fighterName.style.top = '0';

    fighterDescription.innerText = `${fighter.name} HP: ${fighter.health} Attack: ${fighter.attack} Defense: ${fighter.defense}`;
    fighterDescription.style.fontSize = '16px';
    fighterDescription.style.margin = '0';
    fighterName.insertAdjacentElement('beforeend', fighterDescription)

    fighterElement.append(fighterName, fighterImg);
  }

  return fighterElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = {
    src: source,
    title: name,
    alt: name
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}
